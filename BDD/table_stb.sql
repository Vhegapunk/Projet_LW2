create table stb(
  id SERIAL,
  version character(30),
  date character(50),
  titre character(50),
  description character(200),
  commentaire character(200),
  constraint PKSTB primary key(id)
);
 
create table equipe(
  idstb integer,
  nom character(50),
  prenom character(50),
  gender boolean,
  constraint FKEquipeStb foreign key(idstb) references
      stb(id)
);

create table client(
  idstb integer,
  entite character(100),
  nom character(50),
  prenom character(50),
  codeP character(50),
  constraint FKClientStb foreign key(idstb) references
      stb(id)
);

create table fonctionnalite(
  idstb integer,
  id SERIAL,
  priorite integer,
  description character(200),
  constraint PKfonctionnalite primary key(id),
  constraint FKFonctionnaliteStb foreign key(idstb) references
      stb(id)
);

create table exigence(
  idfonc integer,
  id SERIAL,
  priorite integer,
  description character(200),
  constraint PKexigence primary key(id),
  constraint FKExigenceFonctionnalite foreign key(idfonc) references
      fonctionnalite(id)
);
