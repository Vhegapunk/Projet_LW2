<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html lang="en">
<head>
<spring:url value="/resources/css/app.css" var="coreCss" />
<link href="${coreCss}" rel="stylesheet" />
<title>Informations service</title>
</head>
<body>
	<c:if test="${not empty stb}">
	    ${stb.titre}
	    ${stb.date}
	    ${stb.description}
	    ${stb.version}  
	</c:if>
	<c:if test="${not empty lstb}">
		<c:forEach var="stb" items="${lstb}" >
		    ${stb.titre}
		    ${stb.date}
		    ${stb.description}
		    ${stb.version} <br/>
		</c:forEach> 
	</c:if>
	
    <h1 id = "info">Informations</h1>
    <table id = "table_info">
      <tr class = "tr_section">
        <td>URL</td>
        <td>M�thode</td>
        <td>Action</td>
      </tr>
      <tr class = "tr_ligne">
        <td>/STB1/resume</td>
        <td>GET</td>
        <td>renvoie un flux XML contenant la listedes STB</td>
      </tr>
      <tr class = "tr_ligne">
        <td>/STB1/resume/id</td>
        <td>GET</td>
        <td>renvoie un flux XML d�crivant le d�tail de la STB d'identifiant Id</td>
      </tr>
      <tr class = "tr_ligne">
        <td>/STB1/insert</td>
        <td>POST</td>
        <td>
            re�oit un flux XML d�crivant une STB, cr�e l'objet correspondant et
            retourne son nouvel identifiant au format XML
        </td>
      </tr>
    </table>
</body>
</html>