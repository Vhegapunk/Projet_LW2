package model;

import java.net.URI;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import Exception.STBNotFound;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ConnectDB {
    
    private Connection connect = null;
    private PreparedStatement preparedStatement = null;
    
    /**
     * Crée la connection à la base de données.
     */
    private static String DATABASE_URL = "postgres://nbgtrhpoctfpcm:fJgD1Zupp0TXpNFnJF0Ay46eFH@ec2-50-19-117-231.compute-1.amazonaws.com:5432/d6c74iqn1vqdef";
    
    public ConnectDB() throws URISyntaxException {
        URI dbUri = new URI(DATABASE_URL);

        String username = dbUri.getUserInfo().split(":")[0];
        String password = dbUri.getUserInfo().split(":")[1];
        String dbUrl = "jdbc:postgresql://" + dbUri.getHost() + ':' + dbUri.getPort() + dbUri.getPath() + "?ssl=true&sslfactory=org.postgresql.ssl.NonValidatingFactory";
        try {
            connect = DriverManager.getConnection(dbUrl, username, password);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    
    public void DeconnectDB () {
        try {
            connect.close();
        } catch (SQLException ex) {
            Logger.getLogger(ConnectDB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public StbType getStb(int id) throws STBNotFound {
        List<StbType> stbs = new ArrayList<StbType>();
        try {
            preparedStatement = connect
                    .prepareStatement("select * from stb where id = " + id);
            ResultSet result = preparedStatement.executeQuery();
            while (result.next()) {
                int idStb = result.getInt("id");
                preparedStatement = connect
                        .prepareStatement("select * from equipe where idstb = " + idStb);
                ResultSet resultEquipe = preparedStatement.executeQuery();
                List<EquipeType> equipeMembres = new ArrayList<EquipeType>(); 
                while (resultEquipe.next()) {
                    EquipeType membre = new EquipeType();
                    membre.setGender(resultEquipe.getBoolean("gender"));
                    membre.setPrenom(resultEquipe.getString("prenom"));
                    membre.setNom(resultEquipe.getString("nom"));
                    equipeMembres.add(membre);
                }
                preparedStatement = connect
                        .prepareStatement("select * from fonctionnalite where idstb = " + idStb);
                ResultSet resultFonc = preparedStatement.executeQuery();
                List<FonctionnaliteType> fonctionnalites = new ArrayList<FonctionnaliteType>(); 
                while (resultFonc.next()) {
                    FonctionnaliteType fonctionnalite = new FonctionnaliteType();
                    fonctionnalite.setIdentifiant(resultFonc.getInt("id"));
                    fonctionnalite.setDescription(resultFonc.getString("description"));
                    fonctionnalite.setPriorite(resultFonc.getInt("priorite"));
                    int idFonc = resultFonc.getInt("id");
                    preparedStatement = connect
                            .prepareStatement("select * from exigence where idFonc = " + idFonc);
                    ResultSet resultExig = preparedStatement.executeQuery();
                    while (resultExig.next()) {
                        ExigenceType exigence = new ExigenceType();
                        exigence.setIdentifiant(resultExig.getInt("id"));
                        exigence.setDescription(resultExig.getString("description"));
                        exigence.setPriorite(resultExig.getInt("priorite"));
                        fonctionnalite.getExigence().add(exigence);
                    } 
                    fonctionnalites.add(fonctionnalite);
                }
                preparedStatement = connect
                        .prepareStatement("select * from client where idstb = " + idStb);
                ResultSet resultClient = preparedStatement.executeQuery();
                List<ClientType> clients = new ArrayList<ClientType>(); 
                while (resultClient.next()) {
                    ClientType client = new ClientType();
                    ContactType c = new ContactType();
                    c.setNom(resultClient.getString("nom"));
                    c.setPrenom(resultClient.getString("prenom"));
                    client.setCodePostal(resultClient.getString("codeP"));
                    client.setContact(c);
                    client.setEntite(resultClient.getString("entite"));
                    clients.add(client);
                }
                StbType s = new StbType();
                s.setDate(result.getString("date"));
                s.setDescription(result.getString("description"));
                s.setTitre(result.getString("titre"));
                s.setCommentaire(result.getString("commentaire"));
                s.setVersion(result.getString("version"));
                s.getEquipe().addAll(equipeMembres);
                s.getFonctionnalite().addAll(fonctionnalites);
                s.setClient(clients.get(0));
                s.setIdentifiant(idStb);
                stbs.add(s);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if (stbs.size() != 1) {
            throw new STBNotFound();
        }
        return stbs.get(0);
    }
    
    public StbListType getStbs() {
        List<StbType> stbs = new ArrayList<StbType>();
        try {
            preparedStatement = connect
                    .prepareStatement("select * from stb");
            ResultSet result = preparedStatement.executeQuery();
            while (result.next()) {
                int idStb = result.getInt("id");
                preparedStatement = connect
                        .prepareStatement("select * from equipe where idstb = " + idStb);
                ResultSet resultEquipe = preparedStatement.executeQuery();
                List<EquipeType> equipeMembres = new ArrayList<EquipeType>(); 
                while (resultEquipe.next()) {
                    EquipeType membre = new EquipeType();
                    membre.setGender(resultEquipe.getBoolean("gender"));
                    membre.setPrenom(resultEquipe.getString("prenom"));
                    membre.setNom(resultEquipe.getString("nom"));
                    equipeMembres.add(membre);
                }
                preparedStatement = connect
                        .prepareStatement("select * from fonctionnalite where idstb = " + idStb);
                ResultSet resultFonc = preparedStatement.executeQuery();
                List<FonctionnaliteType> fonctionnalites = new ArrayList<FonctionnaliteType>(); 
                while (resultFonc.next()) {
                    FonctionnaliteType fonctionnalite = new FonctionnaliteType();
                    fonctionnalite.setIdentifiant(resultFonc.getInt("id"));
                    fonctionnalite.setDescription(resultFonc.getString("description"));
                    fonctionnalite.setPriorite(resultFonc.getInt("priorite"));
                    int idFonc = resultFonc.getInt("id");
                    preparedStatement = connect
                            .prepareStatement("select * from exigence where idfonc = " + idFonc);
                    ResultSet resultExig = preparedStatement.executeQuery();
                    while (resultExig.next()) {
                        ExigenceType exigence = new ExigenceType();
                        System.out.println(resultExig.getInt("id"));
                        exigence.setIdentifiant(resultExig.getInt("id"));
                        exigence.setDescription(resultExig.getString("description"));
                        exigence.setPriorite(resultExig.getInt("priorite"));
                        fonctionnalite.getExigence().add(exigence);
                    } 
                    fonctionnalites.add(fonctionnalite);
                }
                preparedStatement = connect
                        .prepareStatement("select * from client where idstb = " + idStb);
                ResultSet resultClient = preparedStatement.executeQuery();
                List<ClientType> clients = new ArrayList<ClientType>(); 
                while (resultClient.next()) {
                    ClientType client = new ClientType();
                    ContactType c = new ContactType();
                    c.setNom(resultClient.getString("nom"));
                    c.setPrenom(resultClient.getString("prenom"));
                    client.setCodePostal(resultClient.getString("codeP"));
                    client.setContact(c);
                    client.setEntite(resultClient.getString("entite"));
                    clients.add(client);
                }
                StbType s = new StbType();
                s.setDate(result.getString("date"));
                s.setDescription(result.getString("description"));
                s.setTitre(result.getString("titre"));
                s.setCommentaire(result.getString("commentaire"));
                s.setVersion(result.getString("version"));
                s.getEquipe().addAll(equipeMembres);
                s.getFonctionnalite().addAll(fonctionnalites);
                s.setClient(clients.get(0));
                s.setIdentifiant(idStb);
                stbs.add(s);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            StbListType sl = new StbListType();
            sl.getStb().addAll(stbs);
            return sl;
        }
        StbListType sl = new StbListType();
        sl.getStb().addAll(stbs); 
        return sl;
    }

    public int addSTB(StbType stb) {
        int id = -1;
        try {
            preparedStatement = connect.prepareStatement(
                "insert into  stb(version, date, titre, description, commentaire) values (?, ?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, stb.getVersion());
            preparedStatement.setString(2, stb.getDate());
            preparedStatement.setString(3, stb.getTitre());
            preparedStatement.setString(4, stb.getDescription());
            preparedStatement.setString(5, stb.getCommentaire());
            preparedStatement.executeUpdate();
            try (ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    id = generatedKeys.getInt(1);
                    stb.setIdentifiant(id);
                }
                else {
                    throw new SQLException("Creating stb failed, no ID obtained.");
                }
            }    
        } catch (SQLException e) {
            e.printStackTrace();
        }
    	ClientType c = stb.getClient();
    	for (EquipeType membre : stb.getEquipe()) {
            try {
                preparedStatement = connect.prepareStatement(
                    "insert into equipe values (?, ?, ?, ?)");
                preparedStatement.setInt(1, stb.getIdentifiant());
                preparedStatement.setString(2, membre.getNom());
                preparedStatement.setString(3, membre.getPrenom());
                preparedStatement.setBoolean(4, membre.isGender());
                preparedStatement.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            }
    	}
    	for (FonctionnaliteType f : stb.getFonctionnalite()) {
    	    try {
    	        preparedStatement = connect.prepareStatement(
    	                "insert into fonctionnalite(idstb, priorite, description) values (?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
    	        preparedStatement.setInt(1, stb.getIdentifiant());
    	        preparedStatement.setInt(2, f.getPriorite());
    	        preparedStatement.setString(3, f.getDescription());
    	        preparedStatement.executeUpdate();
                try (ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
                    if (generatedKeys.next()) {
                        int i = generatedKeys.getInt(2);
                        System.out.println("keyFonc = " + i);
                        f.setIdentifiant(i);
                    }
                    else {
                        throw new SQLException("Creating fonctionnalite failed, no ID obtained.");
                    }
                }
                System.out.println(f.getIdentifiant());
    	        for (ExigenceType ex : f.getExigence()) {
    	            preparedStatement = connect.prepareStatement(
    	                    "insert into  exigence(idfonc, priorite, description) values (?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
    	            preparedStatement.setInt(1, f.getIdentifiant());
    	            preparedStatement.setInt(2, ex.getPriorite());
    	            preparedStatement.setString(3, ex.getDescription());
    	            preparedStatement.executeUpdate();
                    try (ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
                        if (generatedKeys.next()) {
                            int i = generatedKeys.getInt(3);
                            ex.setIdentifiant(i);
                            System.out.println("keyExig = " + i);
                        }
                        else {
                            throw new SQLException("Creating fonctionnalite failed, no ID obtained.");
                        }
                    }
    	        }
    	        
    	    } catch (SQLException e) {
    	        e.printStackTrace();
    	    }
    	}
    	try {
            preparedStatement = connect.prepareStatement(
                "insert into  client values (?, ?, ?, ?, ?)");
            preparedStatement.setInt(1, stb.getIdentifiant());
            preparedStatement.setString(2, c.getEntite());
            preparedStatement.setString(3, c.getContact().getNom());
            preparedStatement.setString(4, c.getContact().getPrenom());
            preparedStatement.setString(5, c.getCodePostal());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return id;
    }

    public boolean removeStb(int id) {
        try {
        	preparedStatement = connect.prepareStatement(
        			"delete from equipe where idstb="+id);
        	preparedStatement.executeUpdate();
        	preparedStatement = connect.prepareStatement(
        			"delete from client where idstb="+id);
        	preparedStatement.executeUpdate();
        	preparedStatement = connect
                    .prepareStatement("select * from fonctionnalite where idstb=" + id);
        	ResultSet resultFonc = preparedStatement.executeQuery();
            while (resultFonc.next()) {
            	preparedStatement = connect.prepareStatement(
            			"delete from exigence where idfonc="+resultFonc.getString("id"));
            	preparedStatement.executeUpdate();	
            }
        	preparedStatement = connect.prepareStatement(
        			"delete from fonctionnalite where idstb="+id);
        	preparedStatement.executeUpdate();
        	preparedStatement = connect.prepareStatement(
        			"delete from stb where id="+id);
        	preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
    
   // public static void main(String[] args) {

    /*
            ConnectDB b = null;
            try {
                b = new ConnectDB();
            } catch (URISyntaxException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
            StbType stb = new StbType();
            stb.setCommentaire("le cerveau, un instrument complexe");
            stb.setDate("18/01/2015");
            stb.setDescription("Etude du centre cognitif du cerveau d'une mouche");
            stb.setTitre("Le centre cognitif du cerveau");
            stb.setVersion("2.03");
            ClientType c = new ClientType();
            ContactType co = new ContactType();
            co.setNom("Levasseur");
            co.setPrenom("Henry");
            c.setCodePostal("75100");
            c.setEntite("INSA");
            c.setContact(co);
            stb.setClient(c);
            EquipeType e = new EquipeType();
            e.setGender(true);
            e.setNom("Leroq");
            e.setPrenom("Thomas");
            stb.getEquipe().add(e);
            e = new EquipeType();
            e.setGender(false);
            e.setNom("Foster");
            e.setPrenom("Marie");
            stb.getEquipe().add(e);
            FonctionnaliteType f = new FonctionnaliteType();
            f.setDescription("Simulation d'un neurone");
            f.setPriorite(3);
            ExigenceType ex = new ExigenceType();
            ex.setDescription("Synapses fonctionnelles");
            ex.setPriorite(4);
            f.getExigence().add(ex);
            ex = new ExigenceType();
            ex.setDescription("Liaisons synaptiques modelisées");
            ex.setPriorite(5);
            f.getExigence().add(ex);
            stb.getFonctionnalite().add(f);
            f = new FonctionnaliteType();
            f.setDescription("Simulation d'un réseau de neurones");
            f.setPriorite(6);
            ex = new ExigenceType();
            ex.setDescription("Temps de réponse après l'envoie d'un signal < 10ms");
            ex.setPriorite(7);
            f.getExigence().add(ex);
            ex = new ExigenceType();
            ex.setDescription("Nombre de neurones dans le réseau pouvant atteindre 10 millions");
            ex.setPriorite(2);
            f.getExigence().add(ex);
            stb.getFonctionnalite().add(f);
            b.addSTB(stb);
            */
            /*
            ConnectDB b = null;
            try {
                b = new ConnectDB();
            } catch (URISyntaxException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            StbType stb = b.getStbs().getStb().get(0);
            String stbString = "Identifiant : " + stb.getIdentifiant() + "\n" + 
                    "Titre : " + stb.getTitre() + "\n" +
                    "Date de création : " + stb.getDate() + "\n" + 
                    "Version : " + stb.getVersion() + "\n" + 
                    "Description : " + stb.getDescription() + "\n\n" +
                    "Client : " + "\n" +
                    "\tEntité : " + stb.getClient().getEntite() + "\n" + 
                    "\tContact : " + stb.getClient().getContact().getNom().replaceAll(" ", "") + " " + stb.getClient().getContact().getPrenom().replaceAll(" ", "") + "\n" + 
                    "\tCode Postal : " + stb.getClient().getCodePostal() + "\n\n" + 
                    "Equipe : ";
            for(EquipeType et : stb.getEquipe()){
                stbString += "\t" + et.getNom().replaceAll(" ", "") + " " + et.getPrenom().replaceAll(" ", "") + "\n";
            }
    
            stbString += "\nFonctionnalités : \n";
            for(FonctionnaliteType ft : stb.getFonctionnalite()){
                stbString += "\tIdentifiant : " + ft.getIdentifiant() + "\n" +
                            "\tPriorité : " + ft.getPriorite() + "\n" +
                            "\tDescription : " + ft.getDescription() + "\n" + 
                            "\tExigences : \n";
            for(ExigenceType et : ft.getExigence()){
                stbString += "\t\tIdentifiant : " + et.getIdentifiant() + "\n" + 
                            "\t\tPriorité : " + et.getPriorite() + "\n" + 
                            "\t\tDescription : " + et.getDescription() + "\n\n";
            }
        }
        stbString += "\n";
    
            System.out.println(stbString );
            */
         //}
}
