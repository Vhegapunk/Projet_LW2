//
// Ce fichier a �t� g�n�r� par l'impl�mentation de r�f�rence JavaTM Architecture for XML Binding (JAXB), v2.2.8-b130911.1802 
// Voir <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Toute modification apport�e � ce fichier sera perdue lors de la recompilation du sch�ma source. 
// G�n�r� le : 2016.04.28 � 04:58:07 PM CEST 
//


package model;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour fonctionnaliteType complex type.
 * 
 * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="fonctionnaliteType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="identifiant" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="priorite" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="exigence" type="{http://stb.org}exigenceType" maxOccurs="unbounded" minOccurs="2"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "fonctionnaliteType", propOrder = {
    "identifiant",
    "description",
    "priorite",
    "exigence"
})
public class FonctionnaliteType {

    protected int identifiant;
    @XmlElement(required = true)
    protected String description;
    protected int priorite;
    @XmlElement(required = true)
    protected List<ExigenceType> exigence;

    /**
     * Obtient la valeur de la propri�t� identifiant.
     * 
     */
    public int getIdentifiant() {
        return identifiant;
    }

    /**
     * D�finit la valeur de la propri�t� identifiant.
     * 
     */
    public void setIdentifiant(int value) {
        this.identifiant = value;
    }

    /**
     * Obtient la valeur de la propri�t� description.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * D�finit la valeur de la propri�t� description.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Obtient la valeur de la propri�t� priorite.
     * 
     */
    public int getPriorite() {
        return priorite;
    }

    /**
     * D�finit la valeur de la propri�t� priorite.
     * 
     */
    public void setPriorite(int value) {
        this.priorite = value;
    }

    /**
     * Gets the value of the exigence property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the exigence property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getExigence().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ExigenceType }
     * 
     * 
     */
    public List<ExigenceType> getExigence() {
        if (exigence == null) {
            exigence = new ArrayList<ExigenceType>();
        }
        return this.exigence;
    }

}
