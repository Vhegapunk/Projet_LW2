package controller;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.SchemaFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;
import Exception.STBNotFound;
import model.ConnectDB;
import model.StbListType;
import model.StbType;

import java.net.URL;
import javax.xml.XMLConstants;
import javax.xml.transform.dom.DOMSource;
import javax.xml.validation.Schema;
import javax.xml.validation.Validator;

/**
 * Controller REST.
 * @author antoine
 */
@RestController
public class STBController {
    
    private static final String homeString = "Projet Langage Web 2, Réalisé par : Antoine GILBERT et Hugo LECOMTE.";
    
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String home() {
        // Récupération du nombre de STB dans la base de données.
        ConnectDB stbdao = null;
        try {
            stbdao = new ConnectDB();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        
        int nb = stbdao.getStbs().getStb().size();
        stbdao.DeconnectDB();
        return homeString + "\nNombre de STB : " + nb + ".";
    }
    
    @RequestMapping(value = "/resume", method = RequestMethod.GET)
    public StbListType getListSTB() {
        
        // Connection à la base de données.
        ConnectDB stbdao = null;
        try {
            stbdao = new ConnectDB();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        
        // Récupération de toutes les stb dans la base de données.
        StbListType s = stbdao.getStbs();
        
        for(StbType stb : s.getStb()){
            stb.getEquipe().clear();
            stb.setClient(null);
            stb.getFonctionnalite().clear();
            stb.setCommentaire(null);
        }
        
        stbdao.DeconnectDB();
        return s;
    }

    @RequestMapping(value = "/resume/{id:.+}", method = RequestMethod.GET)
    public ResponseEntity<StbType> getSTB(@PathVariable("id") String id) {
        
        // Connection à la base de données.
        ConnectDB stbdao = null;
        try {
            stbdao = new ConnectDB();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        
        // Récupération de la stb voulu dans la base de données.
        StbType s = new StbType();
        try {
            s = stbdao.getStb(Integer.parseInt(id));
        } catch (STBNotFound ex) {// Si la stb n'existe pas.
            stbdao.DeconnectDB();
            return new ResponseEntity<>(new StbType(),HttpStatus.NOT_FOUND);
        } catch (NumberFormatException n) {// Si l'id n'est pas un nombre.
            stbdao.DeconnectDB();
            return new ResponseEntity<>(new StbType(),HttpStatus.NOT_FOUND);
        }
        
        // Si la stb existe, on la renvoie.
        stbdao.DeconnectDB();
        return new ResponseEntity<>(s,HttpStatus.OK);
    }
    
    @RequestMapping(value="/depot", method=RequestMethod.POST)
    public ResponseEntity<String> addSTB(@RequestBody StbType stb) {
              
        //get file (in my case this is LocationTest under /WEB-INF/classes/..
        URL url = STBController.class.getProtectionDomain().getCodeSource().getLocation();  
        File file = new java.io.File(url.getFile());
        File parent = file.getParentFile();
        while (!"WEB-INF".equals(parent.getName()))
        {
             parent = parent.getParentFile();
        }

        System.out.println(parent.getAbsolutePath());
        
        int id = -1;
        ConnectDB stbdao = null;
        try {
            // Conversion de la stb en flux xml
            File file2 = new File("flux_xml.xml");
            JAXBContext jaxbContext = JAXBContext.newInstance(StbType.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            jaxbMarshaller.marshal(stb, file2);

            // parse an XML document into a DOM tree
            DocumentBuilder parser = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document document = parser.parse(file2);

            // create a SchemaFactory capable of understanding WXS schemas
            SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);

            // load a WXS schema, represented by a Schema instance
            Source schemaFile = new StreamSource(new File(parent.getAbsolutePath() + "/classes/stb.xsd"));
            Schema schema = factory.newSchema(schemaFile);

            // create a Validator instance, which can be used to validate an instance document
            Validator validator = schema.newValidator();

            // validate the DOM tree
            try {
                validator.validate(new DOMSource(document));
                System.out.println("STB VALIDE");
            } catch (SAXException e) {
                System.out.println("STB INVALIDE : " + e.getMessage());
                return new ResponseEntity<String>("STB non valide !",HttpStatus.FORBIDDEN);
            }
            
            // Si le flux xml est valide, ajout de la stb dans la base de donnée
            try {
                stbdao = new ConnectDB();
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
            id = stbdao.addSTB(stb);
        } catch (JAXBException ex) {
            stbdao.DeconnectDB();
            return new ResponseEntity<String>("Erreur serveur !",HttpStatus.I_AM_A_TEAPOT); 
        } catch (SAXException ex) {
            Logger.getLogger(STBController.class.getName()).log(Level.SEVERE, null, ex);
            stbdao.DeconnectDB();
            return new ResponseEntity<String>("Erreur serveur !",HttpStatus.I_AM_A_TEAPOT);
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(STBController.class.getName()).log(Level.SEVERE, null, ex);
            stbdao.DeconnectDB();
            return new ResponseEntity<String>("Erreur serveur !",HttpStatus.I_AM_A_TEAPOT);
        } catch (IOException ex) {
            Logger.getLogger(STBController.class.getName()).log(Level.SEVERE, null, ex);
            stbdao.DeconnectDB();
            return new ResponseEntity<String>("Erreur serveur !",HttpStatus.I_AM_A_TEAPOT);
        }
        stbdao.DeconnectDB();
        return new ResponseEntity<String>("STB correctement ajouté . Identifiant : " + id + ".", HttpStatus.CREATED);
    }
}
