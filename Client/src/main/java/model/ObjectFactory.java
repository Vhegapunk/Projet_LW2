//
// Ce fichier a �t� g�n�r� par l'impl�mentation de r�f�rence JavaTM Architecture for XML Binding (JAXB), v2.2.8-b130911.1802 
// Voir <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Toute modification apport�e � ce fichier sera perdue lors de la recompilation du sch�ma source. 
// G�n�r� le : 2016.04.28 � 04:58:07 PM CEST 
//


package model;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.stb package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Stb_QNAME = new QName("http://stb.org", "stb");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.stb
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link StbType }
     * 
     */
    public StbType createStbType() {
        return new StbType();
    }

    /**
     * Create an instance of {@link ClientType }
     * 
     */
    public ClientType createClientType() {
        return new ClientType();
    }

    /**
     * Create an instance of {@link EquipeType }
     * 
     */
    public EquipeType createEquipeType() {
        return new EquipeType();
    }

    /**
     * Create an instance of {@link FonctionnaliteType }
     * 
     */
    public FonctionnaliteType createFonctionnaliteType() {
        return new FonctionnaliteType();
    }

    /**
     * Create an instance of {@link ContactType }
     * 
     */
    public ContactType createContactType() {
        return new ContactType();
    }

    /**
     * Create an instance of {@link ExigenceType }
     * 
     */
    public ExigenceType createExigenceType() {
        return new ExigenceType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StbType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://stb.org", name = "stb")
    public JAXBElement<StbType> createStb(StbType value) {
        return new JAXBElement<StbType>(_Stb_QNAME, StbType.class, null, value);
    }

}
