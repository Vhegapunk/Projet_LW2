package client;

import model.StbListType;
import model.StbType;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.Date;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import model.ClientType;
import model.ContactType;
import model.EquipeType;
import model.ExigenceType;
import model.FonctionnaliteType;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

public class Client{
    
    private static final String HEROKU = "https://heroku-postgres-59ed0c9b.herokuapp.com";
    private static final String LOCAL = "http://localhost:8080/projet";
    private static final String URL = HEROKU;
    
    private JTextField tGoto;
    private JTextField tURL;
    private JTextArea result;
    private JTextField tSup;
    private JButton bAccueil;
    private JButton bListStb;
    private JButton bGoTo;
    private JButton bAdd;
    private JButton bSup;
    private JFrame mainFrame;
    
    private JPanel getPanel;
    private JPanel postPanel;
    private JTabbedPane onglets;
    private String areaTmp;
    
    // Général
    private JTextField tTitle;
    private JTextField tDescription;
    private JTextField tVersion;
    private JTextField tCommentaire;
    private JButton bGeneral;
    
    // Client
    private JTextField tEntite;
    private JTextField tNomClient;
    private JTextField tPrenomClient;
    private JTextField tCodePostal;
    private JButton bClient;
    
    // Equipe
    private JTextField tNom;
    private JTextField tPrenom;
    private JRadioButton rHomme;
    private JRadioButton rFemme;
    private JButton bEquipe;
    
    // Fonctionnalité
    private JTextField tDescriptionF;
    private JTextField tPrioriteF;
    private JButton bFonctionnalite;
    
    // Exigence
    private JTextField tDescriptionE;
    private JTextField tPrioriteE;
    private JButton bExigence;
    
    // Model
    private StbType actualSTB;
    private FonctionnaliteType actualFonc;

    /**
     * Le s�l�ctionneur de fichier.
     */
    private JFileChooser fileChoose;

    private static final int FRAME_WIDTH = 900;
    private static final int FRAME_HEIGHT = 750;
    
    public Client() {
        createModel();
        createView();
        placeComponents();
        createController();
    }
    
    // COMMANDES
    
    /**
     * Rend l'application visible au centre de l'écran.
     */
    public void display() {
        mainFrame.pack();
        mainFrame.setLocationRelativeTo(null);
        mainFrame.setVisible(true);
        //model.notifyObservers();
    }

    // OUTILS
    
    private void createModel() {
        actualSTB = new StbType();
        actualSTB.setDate(new Date().toString());
    }
    
    private void createView() {
        mainFrame = new JFrame("ClientSTB");
        tGoto = new JTextField("id de la STB voulu",5) ;
        tURL = new JTextField("URL actuelle");
        tSup = new JTextField("id de la STB");
        bAccueil = new JButton("Accueil");
        bListStb = new JButton("Liste STB");
        bGoTo = new JButton("Aller vers la STB");
        bAdd = new JButton("Evoyer STB");
        bSup = new JButton("Réinitialiser STB");
        fileChoose = new JFileChooser();
        result = new JTextArea(30, 50);
        result.setEditable(false);
        tURL.setEditable(false);
        mainFrame.setPreferredSize(new Dimension(FRAME_WIDTH, FRAME_HEIGHT));
        
        onglets = new JTabbedPane();
        getPanel = new JPanel();
        postPanel = new JPanel();
        
        tTitle = new JTextField(5);
        tVersion = new JTextField(5);
        tDescription = new JTextField(5);
        tCommentaire = new JTextField(5);
        bGeneral = new JButton("Modifier");
        
        tEntite = new JTextField(5);
        tNomClient = new JTextField(5);
        tPrenomClient = new JTextField(5);
        tCodePostal = new JTextField(5);
        bClient = new JButton("Modifier");
        
        tNom = new JTextField(5);
        tPrenom = new JTextField(5);
        ButtonGroup group = new ButtonGroup();
        rHomme  = new JRadioButton("Homme");
        rFemme = new JRadioButton("Femme");
        group.add(rHomme);
        group.add(rFemme);
        bEquipe = new JButton("Ajouter");
        
        tDescriptionF = new JTextField(5);
        tPrioriteF = new JTextField(5);
        bFonctionnalite = new JButton("Ajouter");
        
        tDescriptionE = new JTextField(5);
        tPrioriteE = new JTextField(5);
        bExigence = new JButton("Ajouter");
    }
    
    private void placeComponents() {
        JPanel q = new JPanel(new GridLayout(4,1)); {
            JPanel r = new JPanel(new FlowLayout(FlowLayout.CENTER)); {
                JPanel s = new JPanel( new GridLayout(2,2));{
                    s.add(bAccueil);
                    s.add(bListStb);
                    s.add(tGoto);
                    s.add(bGoTo);
                }
                r.add(s);
            }                 
            q.add(r);
        }
        getPanel = q;
        onglets.add(q,"GET Requests");
        
        JPanel p = new JPanel();
        p.setLayout(new BoxLayout(p,BoxLayout.Y_AXIS));{
        
            //Général
            q = new JPanel(new BorderLayout()); {
                JPanel r = new JPanel(new BorderLayout());{
                    JPanel s = new JPanel(new GridLayout(0,2));{
                        s.add(new JLabel("Titre : "));
                        s.add(tTitle);
                        s.add(new JLabel("Version : "));
                        s.add(tVersion);
                        s.add(new JLabel("Descprition : "));
                        s.add(tDescription);
                        s.add(new JLabel("Commentaire : "));
                        s.add(tCommentaire);
                    }
                    r.add(s);
                    s = new JPanel(new FlowLayout());{
                        s.add(bGeneral);
                    }
                    r.add(s, BorderLayout.SOUTH);
                }
                TitledBorder title = BorderFactory.createTitledBorder("Général");
                q.setBorder(title);
                q.add(r);
            }
            p.add(q);
            
            // Client
            q = new JPanel(new BorderLayout());{
                JPanel r = new JPanel(new BorderLayout());{
                    JPanel s = new JPanel(new GridLayout(0,2));{
                        s.add(new JLabel("Entité : "));
                        s.add(tEntite);
                        s.add(new JLabel("Nom client : "));
                        s.add(tNomClient);
                        s.add(new JLabel("Prenom client : "));
                        s.add(tPrenomClient);
                        s.add(new JLabel("Code postal : "));
                        s.add(tCodePostal);
                    }
                    r.add(s);
                    s = new JPanel(new FlowLayout());{
                        s.add(bClient);
                    }
                    r.add(s, BorderLayout.SOUTH);
                }
                TitledBorder title = BorderFactory.createTitledBorder("Client");
                q.setBorder(title);
                q.add(r);
            }
            p.add(q);
            
            // Equipe
            q = new JPanel(new BorderLayout());{
                JPanel r = new JPanel(new BorderLayout());{
                    JPanel s = new JPanel(new GridLayout(0,2));{
                        s.add(new JLabel("Nom : "));
                        s.add(tNom);
                        s.add(new JLabel("Prenom : "));
                        s.add(tPrenom);
                         s.add(rHomme);
                        s.add(rFemme);
                    }
                    r.add(s);
                    s = new JPanel(new FlowLayout());{
                       s.add(bEquipe);
                    }
                    r.add(s, BorderLayout.SOUTH);
                }
                TitledBorder title = BorderFactory.createTitledBorder("Equipe");
                q.setBorder(title);
                q.add(r);
            }
            p.add(q);
            
            // Fonctionnalité
            q = new JPanel(new BorderLayout());{
                JPanel r = new JPanel(new BorderLayout());{
                    JPanel s = new JPanel(new GridLayout(0,2));{
                        s.add(new JLabel("Description : "));
                        s.add(tDescriptionF);
                        s.add(new JLabel("Priorite : "));
                        s.add(tPrioriteF);
                    }
                    r.add(s);
                    s = new JPanel(new FlowLayout());{
                        s.add(bFonctionnalite);
                    }
                    r.add(s, BorderLayout.SOUTH);
                }
                TitledBorder title = BorderFactory.createTitledBorder("Fonctionnalité");
                q.setBorder(title);
                q.add(r);
            }
            p.add(q);
            
            // Exigence
            q = new JPanel(new BorderLayout());{
                JPanel r = new JPanel(new BorderLayout());{
                    JPanel s = new JPanel(new GridLayout(0,2));{
                        s.add(new JLabel("Description : "));
                        s.add(tDescriptionE);
                        s.add(new JLabel("Priorite : "));
                        s.add(tPrioriteE);
                    }
                    r.add(s);
                    s = new JPanel(new FlowLayout());{
                        s.add(bExigence);
                    }
                    r.add(s, BorderLayout.SOUTH);
                }
                TitledBorder title = BorderFactory.createTitledBorder("Exigence");
                q.setBorder(title);
                q.add(r);
            }
            p.add(q);
            
            q = new JPanel( new FlowLayout());{
                q.add(bAdd);
                q.add(bSup);
                /*
                JPanel r = new JPanel(new BorderLayout());{
                    r.add(bSup, BorderLayout.NORTH);
                    r.add(tSup, BorderLayout.SOUTH);
                }
                q.add(r);
                */
            }
            p.add(q);
        }
        postPanel = p;
        onglets.add(p,"POST request");
        
        JScrollPane scrollPane = new JScrollPane(result); 
        
        JPanel r = new JPanel();{
            r.add(onglets);
        }
        
        mainFrame.add(tURL, BorderLayout.NORTH);
        mainFrame.add(r, BorderLayout.EAST);
        mainFrame.add(scrollPane);
    }
    
    private void createController() {
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        bAccueil.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                RestTemplate rest = new RestTemplate();
                String homeString = rest.getForObject(URL, String.class);
                tURL.setText(URL);
                result.setText(homeString);
            }
        });
        
        bListStb.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                tURL.setText(URL + "/resume");String stbListString = "";
                
                // Récupération de la réponse du serveur.
                RestTemplate rest = new RestTemplate();
                //StbListType stblist = rest.getForObject(URL + "/resume", StbListType.class);
                StbListType stblist = rest.getForObject(URL + "/resume", StbListType.class);
                
                // Sinon on affiche la ressource recu.
                for(StbType stb : stblist.getStb()){
                    stbListString += 
                            "Identifiant : " + stb.getIdentifiant() + "\n" + 
                            "Titre : " + stb.getTitre() + "\n" +
                            "Date de création : " + stb.getDate() + "\n" + 
                            "Version : " + stb.getVersion() + "\n" + 
                            "Description : " + stb.getDescription() + "\n" + 
                            "\n--------------------------------------------------------------------------\n";
                }
                result.setText(stbListString);
            }
        });
        
        bGoTo.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                
                tURL.setText(URL + "/resume/" + tGoto.getText());
                
                // Récupération de la réponse du serveur.
                RestTemplate rest = new RestTemplate();
                ResponseEntity<StbType> stb = null;
                try{
                     stb = rest.getForEntity(URL + "/resume/" + tGoto.getText(), StbType.class);
                } catch(HttpClientErrorException ex){ // Si il y a une erreur, on affiche un message d'erreur.
                    if(ex.getStatusCode().equals(HttpStatus.NOT_FOUND)){
                        result.setText("La ressource demandé n'existe pas.");
                    }else{
                        result.setText("Erreur inconnue.");
                    }
                    return;
                }
                 // Sinon on affichage la ressource recu.
                String stbString = "Identifiant : " + stb.getBody().getIdentifiant() + "\n" + 
                            "Titre : " + stb.getBody().getTitre() + "\n" +
                            "Date de création : " + stb.getBody().getDate() + "\n" + 
                            "Version : " + stb.getBody().getVersion() + "\n" + 
                            "Commentaire : " + stb.getBody().getCommentaire() + "\n" + 
                            "Description : " + stb.getBody().getDescription() + "\n\n" +
                            "Client : " + "\n" +
                            "\tEntité : " + stb.getBody().getClient().getEntite() + "\n" + 
                            "\tContact : " + stb.getBody().getClient().getContact().getNom().replaceAll(" ", "") + " " + stb.getBody().getClient().getContact().getPrenom().replaceAll(" ", "") + "\n" + 
                            "\tCode Postal : " + stb.getBody().getClient().getCodePostal() + "\n\n" + 
                            "Equipe : ";
                for(EquipeType et : stb.getBody().getEquipe()){
                    stbString += "\t" + et.getNom().replaceAll(" ", "") + " " + et.getPrenom().replaceAll(" ", "") + "\n";
                }
                
                stbString += "\nFonctionnalités : \n";
                for(FonctionnaliteType ft : stb.getBody().getFonctionnalite()){
                    stbString += "\tIdentifiant : " + ft.getIdentifiant() + "\n" +
                                "\tPriorité : " + ft.getPriorite() + "\n" +
                                "\tDescription : " + ft.getDescription() + "\n" + 
                                "\tExigences : \n";
                    for(ExigenceType et : ft.getExigence()){
                        stbString += "\t\tIdentifiant : " + et.getIdentifiant() + "\n" + 
                                    "\t\tPriorité : " + et.getPriorite() + "\n" + 
                                    "\t\tDescription : " + et.getDescription() + "\n\n";
                    }
                }
                stbString += "\n";
                result.setText(stbString);
                
            }
        });
        
        bSup.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                actualSTB = new StbType();
                actuArea();
            }
        });
        
        bAdd.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                
                RestTemplate rest = new RestTemplate();
                String stbresponse;
                try{
                     stbresponse = rest.postForObject(URL + "/depot", actualSTB, String.class);
                } catch(HttpClientErrorException ex){ // Si il y a une erreur, on affiche un message d'erreur.
                    if(ex.getStatusCode().equals(HttpStatus.FORBIDDEN)){
                        JOptionPane
                            .showMessageDialog(
                            null,
                            "Mauvaise STB !",
                            "Erreur !", JOptionPane.ERROR_MESSAGE);
                    }
                    if(ex.getStatusCode().equals(HttpStatus.I_AM_A_TEAPOT)){
                        JOptionPane
                            .showMessageDialog(
                            null,
                            "Erreur inconnue du serveur.",
                            "Erreur !", JOptionPane.ERROR_MESSAGE);
                    }
                    return;
                }
                result.setText(stbresponse);
                actualSTB = new StbType();
                actualSTB.setDate(new Date().toString());
                
            }
        });
        
        // Général
        bGeneral.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                actualSTB.setCommentaire(tCommentaire.getText());
                actualSTB.setDescription(tDescription.getText());
                actualSTB.setVersion(tVersion.getText());
                actualSTB.setTitre(tTitle.getText());
                actuArea();
            }
        });
        
        // Client
        bClient.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ContactType c = new ContactType();
                c.setNom(tNomClient.getText());
                c.setPrenom(tPrenomClient.getText());
                ClientType cl = new ClientType();
                cl.setContact(c);
                try {
                    Integer.valueOf(tCodePostal.getText());
                    cl.setCodePostal(tCodePostal.getText());
                    cl.setEntite(tEntite.getText());
                    actualSTB.setClient(cl);
                    actuArea();
                } catch (NumberFormatException n) {
                    JOptionPane
                    .showMessageDialog(
                            null,
                            "Le code postal rentrée n'est pas un nombre!",
                            "Erreur !", JOptionPane.ERROR_MESSAGE);
                }

            }
        });
        
        // Equipe
        bEquipe.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                EquipeType eq = new EquipeType();
                if (rFemme.isSelected()) {
                    eq.setGender(false);
                } else if (rHomme.isSelected()) {
                    eq.setGender(true);
                }
                eq.setPrenom(tPrenom.getText());
                eq.setNom(tNom.getText());
                actualSTB.getEquipe().add(eq);
                actuArea();
            }
        });
        
        // Fonctionnalité
        bFonctionnalite.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (actualFonc == null) {
                    try {
                        int i = Integer.valueOf(tPrioriteF.getText());
                        actualFonc = new FonctionnaliteType();
                        actualFonc.setPriorite(i);
                        actualFonc.setDescription(tDescriptionF.getText());
                        actualSTB.getFonctionnalite().add(actualFonc);
                        actuArea();
                    } catch (NumberFormatException n) {
                        JOptionPane
                        .showMessageDialog(
                                null,
                                "La priorité de la fonctionnalité actuelle"
                                + " n'est pas un nombre !",
                                "Erreur !", JOptionPane.ERROR_MESSAGE);
                    }
                } else {
                    int j = JOptionPane.showConfirmDialog(null, "Attention, "
                            + "vous ne pourrez plus rajouter d'exigences à la fonctionnalité actuelle.", "Attention !", JOptionPane.YES_NO_OPTION,
                             JOptionPane.INFORMATION_MESSAGE, null);
                    if (j == 0) {
                        try {
                            int i = Integer.valueOf(tPrioriteF.getText());
                            actualFonc = new FonctionnaliteType();
                            actualFonc.setPriorite(i);
                            actualFonc.setDescription(tDescriptionF.getText());
                            actualSTB.getFonctionnalite().add(actualFonc);
                            actuArea();
                        } catch (NumberFormatException n) {
                            JOptionPane
                            .showMessageDialog(
                                    null,
                                    "La priorité de la fonctionnalité actuelle"
                                    + " n'est pas un nombre !",
                                    "Erreur !", JOptionPane.ERROR_MESSAGE);
                        }
                    }
                }
            }
        });
        
        // Exigence
        bExigence.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (actualFonc == null) {
                    JOptionPane
                    .showMessageDialog(
                            null,
                            "La création d'une éxigence nécecessite de dabord créer une fonctionnalitée !",
                            "Erreur !", JOptionPane.ERROR_MESSAGE);
                } else {
                    try {
                        int i = Integer.valueOf(tPrioriteE.getText());
                        ExigenceType ex = new ExigenceType();
                        ex.setPriorite(i);
                        ex.setDescription(tDescriptionE.getText());
                        actualFonc.getExigence().add(ex);
                        actuArea();
                    } catch (NumberFormatException n) {
                        JOptionPane
                        .showMessageDialog(
                                null,
                                "La priorité de l'exigence actuelle"
                                + " n'est pas un nombre !",
                                "Erreur !", JOptionPane.ERROR_MESSAGE);
                    }
                }
            }
        });
        
        onglets.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent ce) {
                if(onglets.getSelectedIndex() == 1){
                    areaTmp = result.getText();
                    actuArea();
                }
                else {
                    result.setText(areaTmp);
                }
            }
        });
        
    }
    
    private void actuArea() {
        StringBuffer stbString = new StringBuffer(
                        "Titre : " + actualSTB.getTitre() + "\n" +
                        "Date de création : " + actualSTB.getDate() + "\n" + 
                        "Version : " + actualSTB.getVersion() + "\n" + 
                        "Commentaire : " + actualSTB.getCommentaire() + "\n" + 
                        "Description : " + actualSTB.getDescription() + "\n\n");
        if (actualSTB.getClient() != null) {
            stbString.append("Client : " + "\n");           
            if (actualSTB.getClient().getEntite() != null) {
                stbString.append("\tEntité : " + actualSTB.getClient().getEntite() + "\n");
            }  
            if (actualSTB.getClient().getContact() != null) {
                if (actualSTB.getClient().getContact().getNom() != null &&  actualSTB.getClient().getContact().getPrenom() != null) {
                    stbString.append( "\tContact : " + actualSTB.getClient().getContact().getNom().replaceAll(" ", "") 
                            + " " + actualSTB.getClient().getContact().getPrenom().replaceAll(" ", "") + "\n");
                }
            }
            if (actualSTB.getClient().getCodePostal() != null) {
                stbString.append("\tCode Postal : " + actualSTB.getClient().getCodePostal() + "\n\n");
            }
        }
        stbString.append("Equipe : ");
        for(EquipeType et : actualSTB.getEquipe()){
            stbString.append("\t" + et.getNom().replaceAll(" ", "") + " " + et.getPrenom().replaceAll(" ", "") + "\n");
        }

        stbString.append("\nFonctionnalités : \n");
        for(FonctionnaliteType ft : actualSTB.getFonctionnalite()){
            stbString.append("\tPriorité : " + ft.getPriorite() + "\n" +
                    "\tDescription : " + ft.getDescription() + "\n" + 
                    "\tExigences : \n");
            for(ExigenceType et : ft.getExigence()){
                stbString.append("\t\tPriorité : " + et.getPriorite() + "\n" + 
                        "\t\tDescription : " + et.getDescription() + "\n\n");
            }
        }
        stbString.append("\n");
        result.setText(new String(stbString));
    }
    // POINT D'ENTREE
    
    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                new Client().display();
            }
        });
        
    }   
}
